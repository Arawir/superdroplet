#ifndef MODEL_H
#define MODEL_H

#include "itensor/all.h"
#include <string>

using namespace itensor;

MPO hamiltonian(const Boson &sites, Args const& args = Args::global()){
    int M = sites.length();
    double U = args.getReal("U");
    double Unn = args.getReal("Unn");
    bool PBC = args.getBool("PBC");

    auto ampo = AutoMPO(sites);
    // KIN /////////////////////////////
    for(int i=1; i<=(M-1); i++){
        ampo += -1.0,"A",i,"Adag",i+1;
        ampo += -1.0,"Adag",i,"A",i+1;
    }
    if(PBC){
        ampo += -1.0,"A",M,"Adag",1;
        ampo += -1.0,"Adag",M,"A",1;
    }

    // U /////////////////////////////
    if(U*U > 1e-16){
        for(int i=1; i<=M; i++){
            ampo += U,"N",i,"N",i;
        }
    }

    // Unn //////////////////////////////
    if(Unn*Unn > 1e-16){
        for(int i=1; i<=(M-i); i++){
            ampo += Unn,"N",i,"N",i+1;
        }
        if(PBC){
            ampo += Unn,"N",M,"N",1;
        }
    }

    return toMPO(ampo);
}

MPO obsN(const Boson &sites){
    int M = sites.length();

    auto ampo = AutoMPO(sites);
    for(int i=1; i<=M; i++){
        ampo += 1.0,"N",i;
    }

    return toMPO(ampo);
}


std::vector<MPO> obsN_1xM(const Boson &sites){
    std::vector<MPO> out;
    int M = sites.length();

    for(int i=1; i<=M; i++){
        auto ampo = AutoMPO(sites);
        ampo += 1,"n",i;
        out.push_back( toMPO(ampo) );
    }

    return out;
}

std::vector<MPO> obsNN(const Boson &sites){
    std::vector<MPO> out;
    int M = sites.length();

    auto Ncampo = AutoMPO(sites);
    Ncampo += 1,"N",M/2;
    auto Nc = toMPO(Ncampo);

    for(int i=1; i<=M; i++){
        auto ampo = AutoMPO(sites);
        ampo += 1,"N",i;
        out.push_back( nmultMPO(Nc,prime(toMPO(ampo))) );
    }

    return out;
}

#endif //MODEL_H
