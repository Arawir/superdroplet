#include "main.h"
#include "tdvp.h"
#include "basisextension.h"
#include <ctime>

void writeObs(const Boson &sites, const MPS &psi,const MPS &psi0, double time){
    auto H = hamiltonian(sites);
    auto N = obsN(sites);
    auto N_1xM = obsN_1xM(sites);
    auto NcNi = obsNN(sites);

    std::cout << "Time=\t" << time << "\t";
    std::cout << "rtime=\t" << clock()/CLOCKS_PER_SEC << "\t";
    std::cout << "vmemkB=\t" << process_mem_usage() << "\t";
    std::cout << "vmemPeakkB=\t" << process_peak_mem_usage() << "\t";
    std::cout << "dim=\t" << maxLinkDim(psi) << "\t";

    std::cout << "|Ovl|=\t" << std::abs(innerC(psi,psi0)) << "\t";
    std::cout << "E=\t" <<calc(H,psi) << "\t";
    std::cout << "N=\t" <<calc(N,psi) << "\t";

    std::cout << "Ni=\t";
    for(auto &ni : N_1xM){
        std::cout << calc(ni,psi) << "\t";
    }

    std::cout << "NcNi=\t";
    for(auto &ncni : NcNi){
        std::cout << calc(ncni,psi) << "\t";
    }
    std::cout << std::endl;
}

Boson loadSites(Args const& args = Args::global()){
    Boson sites;
    readFromFile( args.getString("startingPsi")+".sites",sites);
    return sites;
}

MPS loadPsi(const Boson &sites,Args const& args = Args::global()){
    MPS psi(sites);
    readFromFile( args.getString("startingPsi")+".psi" ,psi);
    return psi;
}

void timeEvolve(const MPO &H, const MPS &psi0, const Boson &sites){
    Args args = Args::global();
    MPS psi1 = psi0;

    auto energy = real(innerC(psi1,H,psi1));
    printfln("Initial energy = %.5f", energy);

    auto sweeps = Sweeps(1);
    sweeps.maxdim() = args.getInt("maxDim");
    sweeps.mindim() = args.getInt("minDim");
    sweeps.cutoff() = args.getReal("cutoff");
    sweeps.niter() = 80;
    double time = 0.0;
    writeObs(sites,psi1,psi0,time);

    for(int n=1; n<=100000; n++){
       if(n < 3){
           std::vector<Real> epsilonK = {1E-12, 1E-12};
           addBasis(psi1,H,epsilonK,{"Cutoff",args.getReal("cutoff"), "Method","DensityMatrix", "KrylovOrd",3, "DoNormalize",true, "Quiet",true});
        }

       auto tau = args.getReal("dt")*Cplx_i;
       energy = tdvp(psi1,H,tau,sweeps,{"DoNormalize",true, "Quiet",true, "NumCenter",1});
       time += args.getReal("dt");
       writeObs(sites,psi1,psi0,time);
    }
}



int main(int argc, char *argv[]){
    addArgs(argc,argv);

    addArg("PBC",false);
    addArg("U",0.0);
    addArg("Unn",0.0);

    addArg("dt",0.01);

    addArg("ConserveQNs",true);
    addArg("ConserveNb",true);
    addArg("MaxOcc",6);

    addArg("minDim",10);
    addArg("maxDim",800);
    addArg("cutoff",1e-6);
    addArg("nSweepsSave",8);
    addArgS("startingPsi","none");

    Boson sites = loadSites();
    MPS psi0 = loadPsi(sites);
    MPO H = hamiltonian(sites);

    timeEvolve(H, psi0,sites);

    return 0;
}
